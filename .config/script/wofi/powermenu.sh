#!/usr/bin/env bash
#----------------------------------------------------
#  ██████╗  | Powermenu Wofi Script
# ██╔════╝  | ---------------------------------------
# ██║  ███╗ | Created by Gianluca Wolynski
# ██║   ██║ | https://gitlab.com/GianBruco
# ╚██████╔╝ | https://www.reddit.com/user/GianBruco
#  ╚═════╝  | 
#----------------------------------------------------

# Icons
shutdown="襤"
reboot="社"
lock=""
suspend="鈴"
logout=""


# Variable passed to rofi
options="SHUTDOWN\nREBOOT\nLOCK\nSUSPEND\nLOGOUT"
confirm="YES\nNO"

chosen="$(echo -e "$options" | wofi --dmenu -p "Goodbye $USER")"
case $chosen in
    "SHUTDOWN")
		ans=$(echo -e "$confirm" | wofi --dmenu -p "Are You Sure?")
		if [[ $ans == "YES" ]]; then
			systemctl poweroff
		elif [[ $ans == "NO" ]]; then
			exit 0
        fi
        ;;
    "REBOOT")
		ans=$(echo -e "$confirm" | wofi --dmenu -p "Are You Sure?")
		if [[ $ans == "YES" ]]; then
			systemctl reboot
		elif [[ $ans == "NO" ]]; then
			exit 0
        fi
        ;;
    "LOCK")
		if [[ -f /usr/bin/i3lock ]]; then
			gtklock -i
		fi
        ;;
    "SUSPEND")
		ans=$(echo -e "$confirm" | wofi --dmenu -p "Are You Sure?")
		if [[ $ans == "YES" ]]; then
			systemctl suspend
			gtklock -i
		elif [[ $ans == "NO" ]]; then
			exit 0
        fi
        ;;
    "LOGOUT")
		ans=$(echo -e "$confirm" | wofi --dmenu -p "Are You Sure?")
		if [[ $ans == "YES" ]]; then
			#if [[ "$DESKTOP_SESSION" == "Openbox" ]]; then
			#	openbox --exit
			#elif [[ "$DESKTOP_SESSION" == "i3" ]]; then
			#	i3-msg exit
			#elif [[ "$DESKTOP_SESSION" == "Hyprland" ]]; then
				hyprctl dispatch exit
			#fi
		elif [[ $ans == "NO" ]]; then
			exit 0
        fi
        ;;
		
esac
