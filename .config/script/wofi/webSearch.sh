#!/usr/bin/env bash
#----------------------------------------------------
#  ██████╗  | Web Search Wofi Script
# ██╔════╝  | ---------------------------------------
# ██║  ███╗ | Created by Gianluca Wolynski
# ██║   ██║ | https://gitlab.com/GianBruco
# ╚██████╔╝ | https://www.reddit.com/user/GianBruco
#  ╚═════╝  | 
#----------------------------------------------------

# Defining our web browser.
DMBROWSER="${BROWSER:-firefox}"
browser="firefox"

# An array of search engines.
declare -A options
options[DUCKDUCKGO]="https://duckduckgo.com/?q="
options[AMAZON]="https://www.amazon.com/s?k="
options[YOUTUBE]="https://www.youtube.com/results?search_query="
options[REDDIT]="https://www.reddit.com/search/?q="
options[EBAY]="https://www.ebay.com/sch/i.html?&_nkw="
options[TRANSLATE IT_EN]="https://translate.google.com/?sl=it&tl=en&text="
options[TRANSLATE AUTO_IT]="https://translate.google.com/?sl=auto&tl=it&text="
options[URBAN]="https://www.urbandictionary.com/define.php?term="
options[GITHUB]="https://github.com/search?q="
options[IMDB]="https://www.imdb.com/find?q="

# Picking a search engine.
while [ -z "$engine" ]; do
    engine=$(printf '%s\n' "${!options[@]}" | sort | wofi --dmenu -i -p 'Choose search engine:') "$@" || exit
    url="${options["${engine}"]}" || exit
done

# Searching the chosen engine.
while [ -z "$query" ]; do
    query=$(wofi --dmenu -i -L 0 -p "Search on $engine:") "$@" || exit
done

# Display search results in web browser
$browser "$url""$query"
