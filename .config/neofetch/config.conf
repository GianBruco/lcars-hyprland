#██╗      ██████╗ █████╗ ██████╗ ███████╗
#██║     ██╔════╝██╔══██╗██╔══██╗██╔════╝
#██║     ██║     ███████║██████╔╝███████╗
#██║     ██║     ██╔══██║██╔══██╗╚════██║
#███████╗╚██████╗██║  ██║██║  ██║███████║
#╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#
#██╗  ██╗██╗   ██╗██████╗ ██████╗ ██╗      █████╗ ███╗   ██╗██████╗
#██║  ██║╚██╗ ██╔╝██╔══██╗██╔══██╗██║     ██╔══██╗████╗  ██║██╔══██╗
#███████║ ╚████╔╝ ██████╔╝██████╔╝██║     ███████║██╔██╗ ██║██║  ██║
#██╔══██║  ╚██╔╝  ██╔═══╝ ██╔══██╗██║     ██╔══██║██║╚██╗██║██║  ██║
#██║  ██║   ██║   ██║     ██║  ██║███████╗██║  ██║██║ ╚████║██████╔╝
#╚═╝  ╚═╝   ╚═╝   ╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═════╝
#
# By GianBruco
# -------------
# NeoFetch Config
#

print_info() {
    prin "┌───────\n LCARS SETTINGS \n───────┐"
    info "OS" distro
    info "Kernel" kernel
    info "DE" de
    info "WM" wm
    info "Theme" theme
    info "Icons" icons
    info "Shell" shell
    info "Terminal Font" term_font
    info "Memory" memory
    info "Font" font
    info "Uptime" uptime
    prin "└──────────────────────────────┘"
    info cols
}

#--------
# Kernel 
#--------

# Shorten the output of the kernel function.
# Default:  'on'
# Values:   'on', 'off'
# Flag:     --kernel_shorthand
kernel_shorthand="on"


#--------
# Distro 
#--------

# Shorten the output of the distro function
# Default:  'off'
# Values:   'on', 'off', 'tiny'
# Flag:     --distro_shorthand
distro_shorthand="on"

# Show/Hide OS Architecture.
# Default: 'on'
# Values:  'on', 'off'
# Flag:    --os_arch
os_arch="on"


#--------
# Uptime 
#--------

# Shorten the output of the uptime function
# Default: 'on'
# Values:  'on', 'off', 'tiny'
# Flag:    --uptime_shorthand
uptime_shorthand="tiny"


#-------
# Shell 
#-------

# Show the path to $SHELL
shell_path="off"

# Show $SHELL version
shell_version="on"



#--------------------------
# Gtk Theme / Icons / Font 
#--------------------------

# Shorten output of GTK Theme / Icons / Font
# on:  'Numix, Adwaita'
# off: 'Numix [GTK2], Adwaita [GTK3]'
gtk_shorthand="on"

# Enable/Disable gtk2 Theme / Icons / Font
# on:  'Numix [GTK2], Adwaita [GTK3]'
# off: 'Adwaita [GTK3]'
gtk2="off"

# Enable/Disable gtk3 Theme / Icons / Font
# on:  'Numix [GTK2], Adwaita [GTK3]'
# off: 'Numix [GTK2]'
gtk3="on"



#------
# Text 
#------

# Toggle bold text
# Values:   'on', 'off'
# Flag:     --bold
bold="on"

# Info Separator
# Example:
# separator="->":   'Shell-> bash'
# separator=" =":   'WM = dwm'
separator=" -"


#--------
# Colors 
#--------

# Text Colors
# colors=(distro)      - Text is colored based on Distro colors.
# colors=(4 6 1 8 8 6) - Text is colored in the order above.
colors=( 2 2 3 15 3 5)

# Color block range
# Display colors 0-7 in the blocks.  (8 colors)
# neofetch --block_range 0 7
# Display colors 0-15 in the blocks. (16 colors)
# neofetch --block_range 0 15
block_range=(0 15)

# Toggle color blocks
# Values:   'on', 'off'
# Flag:     --color_blocks
color_blocks="on"

# Color block width in spaces
# Values:   'num'
# Flag:     --block_width
block_width=4

# Color block height in lines
# Values:   'num'
# Flag:     --block_height
block_height=1


####################
# Backend Settings #
####################

# Image backend.
# Values:   'ascii', 'caca', 'chafa', 'jp2a', 'iterm2', 'off',
#           'termpix', 'pixterm', 'tycat', 'w3m', 'kitty'
# Flag:     --backend
image_backend="ascii"

# Image Source
# Values:   'auto', 'ascii', 'wallpaper', '/path/to/img', '/path/to/ascii', '/path/to/dir/'
#           'command output (neofetch --ascii "$(fortune | cowsay -W 30)")'
# Flag:     --source
image_source="$HOME/.config/neofetch/ASCII_Starfleet.txt"


#################
# Ascii Options #
#################

# Ascii distro
# Values:  'auto', 'distro_name'
# Flag:    --ascii_distro
# NOTE: Arch and Ubuntu have 'old' logo variants.
#       Change this to 'arch_old' or 'ubuntu_old' to use the old logos.
# NOTE: Ubuntu has flavor variants.
#       Change this to 'Lubuntu', 'Xubuntu', 'Ubuntu-GNOME' or 'Ubuntu-Budgie' to use the flavors.
# NOTE: Arch, Crux and Gentoo have a smaller logo variant.
#       Change this to 'arch_small', 'crux_small' or 'gentoo_small' to use the small logos.
#ascii_distro="auto"

# Ascii Colors
# Values:   'distro', 'num' 'num' 'num' 'num' 'num' 'num'
# Flag:     --ascii_colors
# Example:
# ascii_colors=(distro)      - Ascii is colored based on Distro colors.
# ascii_colors=(4 6 1 8 8 6) - Ascii is colored using these colors.
ascii_colors=(15 9)

# Bold ascii logo
# Whether or not to bold the ascii logo.
# Values:  'on', 'off'
# Flag:    --ascii_bold
ascii_bold="on"
