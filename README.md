# L(inux)CARS inspired Hyprland rice

## (Still work in progress)

I love the world of Star Trek and wanted to create a theme for Hyprland that would graphically reflect the LCARS system with the color of *Star Trek: Next Generation* series, but at the same time be usable on a daily basis.

This is the project **L(inux)CARS for Hyprland** and i have been using it daily for several months now. You can see a preview below:

![](./LCARS_Preview/LCARS_Home.png)

![](./LCARS_Preview/LCARS_Home2.png)


### App Launcher
For the app launcher i use [wofi](https://hg.sr.ht/~scoopta/wofi), a GTK-based customizable launcher for wayland.

![](./LCARS_Preview/LCARS_AppLauncher.png)


### Notifications
For notifications I used the program [swaync](https://github.com/ErikReider/SwayNotificationCenter), a simple notification daemon with a GTK gui for notifications and the control center

![](./LCARS_Preview/LCARS_Notification.png)

### Scripts
I created two scripts to speed up certain actions. One is for shutdown, reboot, lock and logout operations

![](./LCARS_Preview/LCARS_Logout.png)

and the other is used to quickly search for something on preconfigured sites.

![](./LCARS_Preview/LCARS_Search.png)

### Installation

#### Used programs (ArchLinux)
- bluez
- blueman
- grimshot
- xdg-desktop-portal-hyprland 
- swayosd-git
- bluez-utils
- firefox
- gtklock
- hyprland
- kitty
- nemo
- neofetch
- nwg-look
- polkit-gnome
- hyprpaper
- swaync
- wofi
- yay
- ttf-ubuntu-nerd
- ttf-opensans
- waybar-hyprland-git


#### Installation (for ArchLinux Only)

1. Install required programs
```
yay -S --needed bluez blueman grimshot xdg-desktop-portal-hyprland swayosd-git bluez-utils firefox gtklock hyprland kitty nemo neofetch nwg-look polkit-gnome hyprpaper swaync wofi yay ttf-ubuntu-nerd ttf-opensans waybar-hyprland-git`
```

2. Enable `Swayosd` service
```
sudo systemctl enable --now swayosd-libinput-backend.service
```

3. Set `kitty` as default terminal on `Nemo`
```
gsettings set org.cinnamon.desktop.default-applications.terminal exec kitty
```

4. Change line 130 in .config/hypr/hyprland.conf with you username (wofi don't support relative path)


5. Start nwg-look and change font to `Antonio Regular`

6. Add neofetch to your bash config
```
neofetch >> $HOME/.bashrc
```



#### Only for laptop
```
pacman -S brightnessctl xfce4-power-manager
yay -S auto-cpufreq
sudo systemctl enable --now auto-cpufreq.service
sudo systemctl mask power-profiles-daemon.service
```